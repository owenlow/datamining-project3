package process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import au.com.bytecode.opencsv.CSVWriter;


public class TwitterSentimentAnalyzer {

	public static void main(String[] args) throws Exception {
		
		if (args.length > 2) {
			System.setErr(new PrintStream(new File("std.err")));
			String method = args[0];
			if (method.equals("twitter")) {
				System.out.println("Twitter Sentiment Analyzer thing has started");
				System.out.println("Standard error has been redirected to an appropriately named file in your current working directory");
				System.out.println("Please be patient, your request has probably not crashed");
				System.out.println("\n");
				
				String algorithm = args[1];
				String hashTag = args[2];
				String twitterQuery = "https://api.twitter.com/1.1/search/tweets.json?q=%23"
										+ hashTag
										+ "&result_type=recent&lang=en&count=100";
				
				String jsonData = getHTML(twitterQuery);
				String newcsv = hashTag+".csv";
				jsonToTweetCSV(newcsv, jsonData);
				
				TextProcessor p = new TextProcessor();
				if (algorithm.equals("coinflip++")) {
					p.classify(newcsv);
				} else if (algorithm.equals("stanford")) {
					p.stanfordClassify(newcsv);
				} else {
					System.out.println("Invalid algorithm selected");
				}
			} else if (method.equals("csv"))  {
				System.out.println("Reading in from local csv");
				
				String algorithm = args[1];
				String existingcsv = args[2];
				
				TextProcessor p = new TextProcessor();
				if (algorithm.equals("coinflip++")) {
					p.classify(existingcsv);
				} else if (algorithm.equals("stanford")) {
					p.stanfordClassify(existingcsv);
				} else {
					System.out.println("Invalid algorithm, use 'coinflip++' or 'stanford'");
				}
			} else {
				System.out.println("Invalid source, use 'csv' or 'twitter'");
			}
			
			
		} else {
			System.err.println("Insufficient Arguments");
			System.err.println("args: datasource algorithm option");
		}
		

	}
	
	public static String getHTML(String urlToRead) {
		URL url;
		HttpURLConnection conn;
		BufferedReader rd;
		String line;
		String result = "";
		try {
			url = new URL(urlToRead);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Authorization", 
					"Bearer AAAAAAAAAAAAAAAAAAAAAHlBUQAAAAAA7%2B5Up9pmTv7DNiR0OI4fLnmaHC4%3Do9wqa7qBxBigl1pGVKLKRFEorVEhqE2Yhq0aHJcZfSVQyxgs5S");
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = rd.readLine()) != null) {
				result += line;
			}
			rd.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static void jsonToTweetCSV(String filename, String json) throws IOException, ParseException {
		CSVWriter writer = new CSVWriter(new FileWriter(filename));
		JSONParser parser = new JSONParser();
		JSONObject o = (JSONObject) parser.parse(json);
		JSONArray statuses = (JSONArray) o.get("statuses");
		for (Object status : statuses) {
			Long id = (Long) ((JSONObject) status).get("id");
			String text = (String) ((JSONObject) status).get("text");
			writer.writeNext(new String[]{String.valueOf(id), text});
		}
		writer.close();
	}
}
