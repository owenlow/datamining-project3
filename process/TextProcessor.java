package process;

import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import au.com.bytecode.opencsv.CSVReader;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;



public class TextProcessor {
	CSVReader reader = null;
	HashMap<String, Double> weightedWords;
	HashMap<String, String> slangWords;
	ArrayList<String> stopWords;
	
	private static final String WORD_FILE = "/resources/AFINN-111.txt";
	private static final String LABELED_TWEETS = "/resources/labelled.csv";
	private static final String SLANG_FILE = "/resources/slang";
	private static final String STOP_FILE = "/resources/stop";
	
	public TextProcessor() {
		this(WORD_FILE, SLANG_FILE, STOP_FILE);
	}
	
	public TextProcessor(String weightedWordFile, String slangWordFile, String stopWordFile) {
		weightedWords = new HashMap<>();
		slangWords = new HashMap<>();
		stopWords = new ArrayList<String>();
		String[] line = null;
		try {
			reader = new CSVReader(
					new InputStreamReader(getClass().getResourceAsStream(weightedWordFile)), '\t');
			line = reader.readNext();
			while (line != null) {
				weightedWords.put(line[0], Double.valueOf(line[1]));
				line = reader.readNext();
			}
			
			reader = new CSVReader(
					new InputStreamReader(getClass().getResourceAsStream(slangWordFile)), '-');
			line = reader.readNext();
			while (line != null) {
				slangWords.put(line[0], (line[1]));
				line = reader.readNext();
			}
			
			reader = new CSVReader(
					new InputStreamReader(getClass().getResourceAsStream(stopWordFile)), '-');
			line = reader.readNext();
			while (line != null) {
				stopWords.add(line[0]);
				line = reader.readNext();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void stanfordClassify(String testFile) {
		int positiveInstances = 0;
		int neutralInstances = 0;
		int negativeInstances = 0;
		int instanceCount = 0;
		
		try {

			CSVReader reader = new CSVReader(
					new FileReader(new File(testFile))
					);
			String[] line = null;
			line = reader.readNext();
			while(line != null) {
				String text = line[1];
				Properties props = new Properties();
		        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
		        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		        int mainSentiment = 0;
		        if (text != null && text.length() > 0) {
		            int longest = 0;
		            Annotation annotation = pipeline.process(text);
		            for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
		                Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
		                int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
		                String partText = sentence.toString();
		                if (partText.length() > longest) {
		                    mainSentiment = sentiment;
		                    longest = partText.length();
		                }
		 
		            }
		        }
		        ++instanceCount;
		        if (mainSentiment > 2) {
		        	++positiveInstances;
		        	System.err.println(text + ", pos");
		        } else if (mainSentiment < 2) {
		        	++negativeInstances;
		        	System.err.println(text + ", neg");
		        } else {
		        	++neutralInstances;
		        	System.err.println(text + ", neut");
		        }
		        line = reader.readNext();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Results:");
		System.out.println("Instances: " + instanceCount);
		System.out.println("Positive Instances: " + positiveInstances);
		System.out.println("Neutral Instances: " + neutralInstances);
		System.out.println("Negative Instances: " + negativeInstances);
		System.out.println("Percent positive (excluding neutral): " + (int)(100*(float)((float)positiveInstances)/((float)instanceCount-neutralInstances)));
	}
	
	public void classify(String testFile) {
		int trainPercent = 100;
		int testPercent = 100 - trainPercent;
		
		train(LABELED_TWEETS, trainPercent);
		test(testFile, testPercent);
	}
	
	public void classify(String trainFile, String testFile) {
		int trainPercent = 100;
		int testPercent = 100 - trainPercent;
		
		train(trainFile, trainPercent);
		test(testFile, testPercent);
	}
	
	public void classify(String file, Integer percentSplit) {
		int testPercent = 100 - percentSplit;
		
		train(file, percentSplit);
		test(file, testPercent);
	}
	
	public void train(String filename, Integer percentSplit) {

		try {
			CSVReader reader = new CSVReader(new InputStreamReader(getClass().getResourceAsStream(filename)));
			List<String[]> lines = reader.readAll();
			
			int stop = new Double(lines.size()*percentSplit/100).intValue();
			
			for(int i = 0; i < stop; i++) {
				String[] line = lines.get(i);
				Double score = new Double(0);
				String msg = line[1].substring(1, line[1].length()-1);
				List<String> tokens = tokenize(msg);
				List<String> wordsUsed = new ArrayList<String>();
				
				List<String> words = new ArrayList<String>();
				for(String token : tokens) {

					//remove punctuation
					String word = token.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
					
					//check for stop word
					if (stopWords.contains(word)) {
						continue;
					}
					words.add(word);
				}
				
				for(String s : words) {
					Double wordscore = weightedWords.get(s);
					if (wordscore != null) {
						score += wordscore;
						wordsUsed.add(s);
					}
				}
				
				boolean posLabel = line[2].equals("pos") ? true : false;
				boolean posCalculated = score >= 0 ? true:false;
				
				//if the prediction was wrong
				if (posLabel != posCalculated) {
					for(String s : words) {
						if (!weightedWords.keySet().contains(s)) {
							weightedWords.put(s,score/words.size());
						}
					}

					//update the word weights
					Double scoreChange = ((score/(wordsUsed.size())));
					for(String s : wordsUsed) {
						Double newScore = weightedWords.get(s) - scoreChange;
						weightedWords.put(s, newScore);
					}

				}
			}

			reader.close();

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void test(String filename, Integer inversePercentSplit) {
		int positiveInstances = 0;
		int neutralInstances = 0;
		int negativeInstances = 0;
		
		int instanceCount = 0;
		try {
			CSVReader reader = new CSVReader(new FileReader(filename));
			List<String[]> lines = reader.readAll();
			
			int stop = new Double(lines.size()*inversePercentSplit/100).intValue();
			
			for(int i = stop; i < lines.size(); i++) {
				String[] line = lines.get(i);
				Double score = new Double(0);
				
				String msg = line[1].substring(1, line[1].length()-1);
				List<String> tokens = tokenize(msg);
				

				List<String> words = new ArrayList<String>();
				for(String token : tokens) {

					//remove punctuation
					String word = token.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
					
					//check for stop word
					if (stopWords.contains(word)) {
						continue;
					}
					words.add(word);
				}
				for (String s : words) {

					Double wordscore = weightedWords.get(s);
					if (wordscore != null) {
						score += wordscore;
					}
				}
				

				// if the read in label is the same positive/negative as our score, increment 
				// agreeingInstances

				line = reader.readNext();
				if (score > 0.5) {
					++positiveInstances;
					System.err.println(msg + ", pos");
				} else if (score < -0.5) {
					++negativeInstances;
					System.err.println(msg + ", neg");
				} else {
					++neutralInstances;
					System.err.println(msg + ", neut");
				}
				++instanceCount;
				
			}
			
			reader.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Results:");
		System.out.println("Instances: " + instanceCount);
		System.out.println("Positive Instances: " + positiveInstances);
		System.out.println("Neutral Instances: " + neutralInstances);
		System.out.println("Negative Instances: " + negativeInstances);
		System.out.println("Percent positive (excluding neutral): " + (int)(100*(float)((float)positiveInstances)/((float)instanceCount-neutralInstances)));
	}
	
	public static List<String> tokenize(String s) {
	    PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<CoreLabel>(
	            new StringReader(s), new CoreLabelTokenFactory(), "");
	    List<String> sentence = new ArrayList<String>();
	    StringBuilder sb = new StringBuilder();
	    for (CoreLabel label; ptbt.hasNext();) {
	        label = ptbt.next();
	        String word = label.word();
	        if (word.startsWith("'")) {
	            sb.append(word);
	        } else {
	            if (sb.length() > 0)
	                sentence.add(sb.toString());
	            sb = new StringBuilder();
	            sb.append(word);
	        }
	    }
	    if (sb.length() > 0)
	        sentence.add(sb.toString());
	    return sentence;
	}

}
